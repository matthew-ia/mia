const airbnb = require('@neutrinojs/airbnb');
const react = require('@neutrinojs/react');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const images = require('@neutrinojs/image-loader');


module.exports = {

  options: {
    root: __dirname,
  },
  use: [
    // TODO: turn this back on, fix linter errors
    // airbnb({
    //   eslint: {
    //     // For supported options, see:
    //     // https://github.com/webpack-contrib/eslint-loader#options
    //     // https://eslint.org/docs/developer-guide/nodejs-api#cliengine
    //     // The options under `baseConfig` correspond to those
    //     // that can be used in an `.eslintrc.*` file.
    //     baseConfig: {
    //       rules: {
    //         'babel/semi': 'off',
    //         // 'react/jsx-one-expression-per-line': ['warn', {'allow': 'single-child'}],
    //         'react/jsx-one-expression-per-line': 'off',
    //         'react/destructuring-assignment': 'warn',
    //         'react/state-in-constructor': 'off',
    //         'react/forbid-prop-types': 'off',
    //         'import/prefer-default-export': 'off',
    //         // 'prefer-destructuring': 'warn', /*['warn', { "array": true, "object": true}*/
    //       },
    //     },
    //   },
    // }),
    // eslint({
    //   eslint: {
    //     // Use own `.eslintrc.*` file for configuration instead.
    //     useEslintrc: true,
    //   },
    // }),
    react({
      html: {
        title: 'mia2020'
      },
      style: {
        loaders: [
          {
            loader: 'sass-loader',
            useId: 'sass',
          },
          // {loader: 'style-loader', options: { injectType: 'linkTag' }}
        ],
        test: /\.(css|sass|scss)$/,
        modulesTest: /\.module\.(css|sass|scss)$/,
        // modules: true,
        // extract: {
        //   enabled: process.env.NODE_ENV === 'production',
        //   // // loader: {
        //   //   esModule: true,
        //   // },
        //   plugin: {
        //     filename:
        //       process.env.NODE_ENV === 'production'
        //         ? 'assets/[name].[contenthash:8].css'
        //         : 'assets/[name].css',
        //   },
        // },
      },
      image: {
        // limit: 8192,
        name:
          process.env.NODE_ENV === 'production'
            ? '/assets/img/[name].[ext]'
            : 'assets/img/[name].[ext]',
      },
      font: {
        name:
          process.env.NODE_ENV === 'production'
            ? '/assets/fonts/[name].[ext]'
            : 'assets/fonts/[name].[ext]',
      }
    }),
    (neutrino) => {
      // the original value of filename is "[name].[chunkhash].js"
      neutrino.config.output.filename('[name].bundle.js');
      neutrino.config.output.publicPath('');
    },
  ],
};

// https://github.com/neutrinojs/neutrino/issues/803
