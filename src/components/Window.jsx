import React, { Component } from 'react';
import PropTypes from 'prop-types';
import WindowTitleBar from './WindowTitleBar';

import setDraggable , { unsetDraggable } from '../utilities/setDraggable';

class Window extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // isActive: true,
    };

    // Set Refs
    this.windowRef = React.createRef();
    this.titleBarRef = null;
    this.setTitleBarRef = (element) => {
      this.titleBarRef = element;
    };
  }

  componentDidMount() {
    // Using the refs, set the Window to be draggable
    // with WindowTitleBar as its drag handle
    if (!this.props.isMobile)
      setDraggable(this.windowRef.current, this.titleBarRef);
    // else
    //   setDraggable(this.windowRef.current, this.titleBarRef, true);
  }

  render() {
    // if (this.props.isMobile) {
    //   setDraggable(this.windowRef.current, this.titleBarRef, true); // unset
    // }
    const { children, onWindowClose, data, handleFocusWindow, isActive, isMobile } = this.props;
    let { orderIndex } = this.props;
    orderIndex += 1;
    const zIndex = {
      zIndex: (orderIndex * 10),
    }
    // if (isMobile) unsetDraggable('');
    // TODO: add dynamic position styling
    // if activeWindow has style.transform, assume it's moved out of the Start
    // position, and send this one there.
    // if not, then take the default pos and increment top/right so it's shifted
    // down from the activeWindow

    let modifierClass = '';
    if (isActive) modifierClass = ' Window--active';
    return (
      <div className={`Window${modifierClass}`}
           id={data.id}
           ref={this.windowRef}
           style={zIndex}
           onFocus={()=>{handleFocusWindow(data.id)}}
           tabIndex={orderIndex}> {/* TODO: consider removing for a11y*/}
        <WindowTitleBar titleBarRef={this.setTitleBarRef}
                        onWindowClose={() => onWindowClose(data.id)}
                        title={data.title} />
        <div className="Window__Inner">
          {children}
        </div>
      </div>
    );
  }
}

export default Window;

Window.propTypes = {
  children: PropTypes.object, // the Window's content
}

Window.defaultProps = {
  children: {},
}
