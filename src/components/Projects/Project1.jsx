import React from 'react';
import Image from '../Image';
import { assets } from '../../static/img/p1';
import { placeholders } from '../../static/img';
// import { default as p } from '../../static/img/placeholder/placeholder-intro.png';

function Project1(props) {
  let { data, isMobile } = props;
  let math = ((window.innerWidth * .7) - (36 * 2)) / 2.96;
  let preloadStyle = {
    minHeight: `${math}px`,
  }
  return (
    <>
      <header className="section">
        <div className="row">
          <div className="col">
            <div className="header">
              <h1 className="header-title">Project Title</h1>
              <div className="header-meta">
                <div className="header-time">
                  <span className="time-start">2019</span>
                  <span className="time-arrow">--></span>
                  <span className="time-end">Now</span>
                </div>
                <ul className="header-tag-list">
                  <li className="header-tag">Tag1</li>
                  <li className="header-tag">Tag2</li>
                  <li className="header-tag">Tag3</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </header>
      <section className="section intro">
        <div className="row">
          <div className="col">
            <Image style={preloadStyle}
              src={placeholders[0]}
              parentData={data}
              isMobile={isMobile}
              isLandscape />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <p className="lead">Spectra is a visual search engine that lets
              users interact with their query and its results in new ways.</p>
          </div>
        </div>
      </section>
      <section className="section">
        <h2 className="section-heading">01 Section</h2>
        <div className="section-content">
          <div className="row">
            <div className="col">
              <p className="text">some text with an <a href="#" className="link">outlink</a> in the middle of the text ok ok ok.</p>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <Image
                src={assets[0]}
                parentData={data}
                isMobile={isMobile}
                isLandscape />
            </div>
          </div>
          <div className="row">
            <div className="col-50">
              <Image
                src={placeholders[1]}
                parentData={data}
                isMobile={isMobile}
                isPortrait />
            </div>
            <div className="col-50">
              <Image
                src={assets[0]}
                parentData={data}
                isMobile={isMobile}
                isLandscape />
            </div>
          </div>
        </div>
      </section>
      <section className="footer">
        <div className="footer-section">
          <h3 className="footer-section-heading">
            Tools & Technologies
          </h3>
          <ul className="footer-list-inline">
            <li className="footer-list-item">React</li>
            <li className="footer-list-item">JS</li>
            <li className="footer-list-item">SASS</li>
            <li className="footer-list-item">Tool</li>
            <li className="footer-list-item">Technology</li>
          </ul>
        </div>
        <div className="footer-section">
          <h3 className="footer-section-heading">
            Resources
          </h3>
          <ul className="footer-list">
            <li className="footer-list-item">
              <a className="outlink" href="#">Source Code</a>
            </li>
            <li className="footer-list-item">
              <a className="outlink" href="#">Demo</a>
            </li>
          </ul>
        </div>
      </section>
    </>
  );
}

export default Project1;
