import React, { Component } from 'react';
import Tippy from '@tippyjs/react';
import 'tippy.js/dist/tippy.css'; // optional
import 'tippy.js/animations/shift-toward.css';

import Icon from './Icon';
import IconWithTooltip from './Icon/IconWithTooltip';
import { getFormattedTime } from '../utilities/time';

class StatusBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: getFormattedTime(),
    }
    this.clockTick = this.clockTick.bind(this);
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.clockTick(),
      3000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  clockTick() {
    this.setState({
      time: getFormattedTime(),
    })
  }

  render() {
    let { path, handleOpenWindow, openFloatingMenu, isWindowStackEmpty, isMobile } = this.props;
    // Format path string
    let pathString = '';
    if (path) {
      path = path.split('/');
      for (let i = 0; i < path.length; i++) {
        if (i == path.length-1) pathString += path[i];
        else pathString += `${path[i]} / `
      }
    }

    let handleFloatingMenu = openFloatingMenu;
    if (isMobile && isWindowStackEmpty) {
      handleFloatingMenu = ()=>{
        //TODO: pulse window
        console.log("TODO: pulse window");
      };
    }

    const tippyTheme = 'desktop-theme';
    // If mobile, disable tippy trigger, else use default
    const tippyTrigger = ((isMobile == true) ? 'manual' : 'mouseenter focus');
    const tippyProps = {
      // For debugging:
      // hideOnClick: false,
      // trigger: 'click',
      trigger: tippyTrigger,
      arrow: false,
      duration: [450, 175],
      offset: [0, 24],
      animation: 'shift-toward',
      touch: false,
      // inertia: true,
    }

    return (
      <div className="StatusBar">
        <div className="StatusBar__CWD">
          <div className="StatusBar__CWDInner">
            <div>matthewia<span>{pathString ? ' /' : ''} {pathString}</span></div>
          </div>
        </div>
        <div className="StatusBar__Nav">
          <div className="StatusBar__Info" onClick={handleOpenWindow.bind(this, 'wInfo')}>
            <Tippy content="About" className={tippyTheme} {...tippyProps}>
              {}
              <IconWithTooltip icon="info" />
            </Tippy>
          </div>
          <div className="StatusBar__Menu" onClick={handleFloatingMenu}>
            <Icon icon="menu" isDisabled={(isMobile && isWindowStackEmpty)}/>
          </div>
          {/* FIXME: add a click listner for the mail link */}
          <div className="StatusBar__Message" onClick={handleOpenWindow.bind(this, 'wContact')}>
            <Tippy content="Message me!" className={tippyTheme} {...tippyProps} offset={[0,26]}>
              <IconWithTooltip icon="message" />
            </Tippy>
          </div>
        </div>
        <div className="StatusBar__Clock">{this.state.time}</div>
      </div>

    );
  }

}

export default StatusBar;
