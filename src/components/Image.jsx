import React, { Component } from 'react';
import SubWindow from './SubWindow';

class Image extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubWindowOpen: false,
    }
    this.handleSubWindow = this.handleSubWindow.bind(this);
  }

  handleSubWindow(open) {
    if (this.props.isMobile) return;
    if (open) {
      this.setState({
        isSubWindowOpen: true,
      });
    } else {
      this.setState({
        isSubWindowOpen: false,
      });
    }
  }

  render() {
    const { src, parentData, isLandscape, isPortrait, style } = this.props;
    const { isSubWindowOpen } = this.state;

    let modifierClass = '';
    if (isLandscape)
      modifierClass = ' Image--landscape';
    else if (isPortrait)
      modifierClass = ' Image--portrait';

    const img = (
      <img
        style={style}
        className={`Image${modifierClass}`}
        src={src}
        onMouseDown={()=>{this.handleSubWindow(1)}}
      />
    );

    let subWindow = null;
    if (isSubWindowOpen) subWindow = (
      <SubWindow
        src={src}
        parentData={parentData}
        onWindowClose={()=>{this.handleSubWindow(0)}}
        isLandscape={isLandscape}
        isPortrait={isPortrait}>
        {img}
      </SubWindow>
    );

    return (
      <>
        {img}
        {subWindow}
      </>

    );
  }

}

export default Image;
