import React from 'react';
// import close from '../../static/icons/close.svg';
// import * as icons from './icons';
import * as icons from '../../static/icons';

function Icon(props) {
  const { icon, className, isDisabled } = props;
  let iconSvg = null;
  switch (icon) {
    case 'close':
      iconSvg = icons.close;
      break;
    case 'folder':
      iconSvg = icons.folder;
      break;
    case 'page':
      iconSvg = icons.page;
      break;
    case 'info':
      iconSvg = icons.info;
      break;
    case 'menu':
      iconSvg = icons.menu;
      break;
    case 'message':
      iconSvg = icons.message;
      break;
    default: break;
  }

  let classNames = `Icon Icon--${icon}`;
  if (className) classNames += ` ${className}`;
  if (isDisabled) classNames += ` Icon--disabled`;

  return (
    <div className={classNames}>
      {iconSvg}
    </div>
  );
}

export default Icon;

// export default React.forwardRef((props, ref) => {
//   return <Icon {...props} innerRef={ref} />
// });
