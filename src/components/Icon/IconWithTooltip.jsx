import React from 'react';
import Icon from './Icon';

function IconWithTooltip(props) {
  return (
    <div ref={props.innerRef}>
      <Icon {...props} />
    </div>
  );
}

export default React.forwardRef((props, ref) => {
  return <IconWithTooltip {...props} innerRef={ref} />
});
