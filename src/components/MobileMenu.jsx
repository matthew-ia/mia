import React, { Component } from 'react';

class MobileMenu extends Component {
  constructor(props) {
    super(props);

    this.openWindow = this.openWindow.bind(this);
  }

  openWindow(id) {
    const { activeWindow, handleOpenOneWindow } = this.props;
    if (activeWindow.id === id) return; // already open, do nothing
    handleOpenOneWindow(id);
  }

  render() {
    // const { handleOpenWindow } = this.props;

    return (
      <div className="MobileMenu">
        <div className="MobileMenu__TitleBar">
          <div className="MobileMenu__Title">MobileMenu</div>
          <div className="MobileMenu__Controls">
            ...
            {/* <div className="Window__Close" onClick={() => onWindowClose(id)}>x</div> */}
          </div>
        </div>
        <ul className="MobileMenu__List">
          <li
            className="MobileMenu__ListItem"
            onClick={() => this.openWindow('p1')}>
              <span className="MobileMenu__ListItemInner">Project 1<i>-></i></span>
          </li>
          <li
            className="MobileMenu__ListItem"
            onClick={() => this.openWindow('p2')}>
              <span className="MobileMenu__ListItemInner">Project 2<i>-></i></span>
          </li>
          <li
            className="MobileMenu__ListItem"
            onClick={() => this.openWindow('p3')}>
              <span className="MobileMenu__ListItemInner">Project 3<i>-></i></span>
          </li>
        </ul>
      </div>
    );
  }

}

export default MobileMenu;
