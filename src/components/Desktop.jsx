import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import WindowManager from './WindowManager';
import FloatingMenu from './FloatingMenu';
import StatusBar from './StatusBar';
// Utilities
import * as rutils from '../utilities/routerUtils';
import * as wutils from '../utilities/windowUtils';
import preload from '../utilities/preload';

class Desktop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      windowList: [],   // Controls which Windows should be rendered
      windowStack: [],  // Controls the stack order of the open Windows
      activeWindow: {},
      isMobile: false,
      isFloatingMenuOpen: true,
    };

    // Methods
    this.handleOpenWindow = this.handleOpenWindow.bind(this);
    this.handleCloseWindow = this.handleCloseWindow.bind(this);
    this.setActiveWindow = this.setActiveWindow.bind(this);
    this.handleFocusWindow = this.handleFocusWindow.bind(this);
    this.getWindowData = this.getWindowData.bind(this);
    this.handleOpenOneWindow = this.handleOpenOneWindow.bind(this);
    this.handleNavigateRoute = this.handleNavigateRoute.bind(this);
    this.checkIsMobile = this.checkIsMobile.bind(this);
    this.openFloatingMenu = this.openFloatingMenu.bind(this);
  }

  componentDidMount() {
    this.router = rutils.setupRouter(this.handleRoute);
    // Ensure mounted hash is valid
    // if invalid, this call will also run router.navigateError
    if (this.router.validateCurrentRoute() && this.router.currentRoute !== '') {
      const name = this.router.parseRoute(this.router.currentRoute).slice(-1)[0];
      const w = this.props.data.find((w) => w.name === name);
      console.log(w, name);
      this.handleOpenOneWindow(w.id);
    }
    // Start listening once we've handled the route we mounted with
    this.router.listen();
    window.addEventListener('resize', this.checkIsMobile);
    this.checkIsMobile();
    preload();

    // Set height instead of using vh
    // This avoids issues with some mobile browser UIs
    // if (this.checkIsMobile())
    //   document.querySelector('.Desktop').style.height = `${window.innerHeight}px`;
  }

  checkIsMobile() {
    if (window.innerWidth <= 992) {
      this.setState({
        isMobile: true,
      });
    } else {
      this.setState({
        isMobile: false,
      });
    }
    return (window.innerWidth <= 992);
  }

  setActiveWindow(w) {
    if (this.state.activeWindow.id === w.id) {
      return; // return if already active
    }
    // Window type check
    const temp = [...this.state.windowStack];
    const wIndex = this.state.windowStack.findIndex((wId) => wId === w.id);

    // Remove Window from it's current index
    temp.splice(wIndex, 1);
    // Add Window to the top of the "stack" (windowStack)
    temp.push(w.id);

    this.setState({
      windowStack: temp,
      activeWindow: w,
    });
    this.handleNavigateRoute(w);
    // document.querySelector(`#${w.id}`)
    //   .classList.remove('Window--default');
  }

  handleFocusWindow(id) {
    const { data } = this.props;
    const w = this.getWindowData(id);
    this.setActiveWindow(w);
  }

  handleOpenWindow(id, options) {
    if (options.onlyOne) {
      this.handleOpenOneWindow(id);
      return;
    }
    const { data } = this.props;
    const w = this.getWindowData(id);

    if (w) {
      // If found, then Window is already open
      this.setActiveWindow(w);
    } else {
      // Initialize a new window
      let newWindow = this.props.data.find(d => d.id === id);
      if (newWindow) {
        this.setState(prevState => ({
          windowList: [...prevState.windowList, newWindow],
          windowStack: [...prevState.windowStack, newWindow.id],
          activeWindow: newWindow,
        }));
        this.handleNavigateRoute(newWindow);
      } else {
        // Can't find window, check if it's an asset subwindow
        if (wutils.getWindowType(id) === wutils.types.ASSET) {
          // If asset subwindow, create a window data object for it
          newWindow = wutils.generateWindowData(id);
          console.log("new window:", newWindow);
          this.setState(prevState => ({
            windowList: [...prevState.windowList, newWindow],
            windowStack: [...prevState.windowStack, newWindow.id],
            activeWindow: newWindow,
          }));
          // rutils.addSubRoute(newWindow.name);
        }
      }
    }
  }

  handleOpenOneWindow(id) {
    // Reset windowList and windowStack to only include the window to open
    const newWindow = this.props.data.find(d => d.id === id);
    this.setState({
      windowList: [newWindow],
      windowStack: [newWindow.id],
      activeWindow: newWindow,
    });
    this.handleNavigateRoute(newWindow);
  }

  handleNavigateRoute(w) {
    // Catch if w is undefined
    // this means that there is no window to render next
    if (!w) {
      this.router.navigate();
      this.setState({
        activePath: '',
      });
      return;
    }

    // Navigate to the proper route based on the window type
    let path = '';
    if (wutils.getWindowType(w.id) === wutils.types.PROJECT) {
      path = `work/${w.name}`;
      this.router.navigate(path);

    } else {
      path = w.name;
      this.router.navigate(w.name);
    }
    this.setState({
      activePath: path,
    });
  }

  handleCloseWindow(id) {
    // Create copy of windowList to manipulate
    let tempWindowList = [...this.state.windowList];
    let wListIndex = tempWindowList.findIndex(w => w.id === id);
    tempWindowList.splice(wListIndex, 1);

    let tempWindowStack = [...this.state.windowStack];
    let wStackIndex = tempWindowStack.findIndex(wId => wId === id);
    tempWindowStack.splice(wStackIndex, 1);

    // TODO: add close animation trigger here, wait a few sec
    // https://reactjs.org/docs/animation.html
    // Wrap Window component with ReactCSSTransitionGroup

    // Get last Window from temp, after the Window has been closed
    // This will be set as the new activeWindow
    const [lastItem] = tempWindowList.slice(-1);
    console.log('closing', id);
    if (lastItem) {
      this.setState({
        windowList: tempWindowList,
        windowStack: tempWindowStack,
        activeWindow: lastItem,
      });
      // if (wutils.getWindowType(id) === wutils.types.ASSET)
      //   rutils.removeSubRoute();
      this.handleNavigateRoute(lastItem);
    } else {
      // lastItem doesn't exist, so stack must be empty
      this.setState({
        windowList: [],
        windowStack: [],
        activeWindow: {},
      });
      // if (wutils.getWindowType(id) === wutils.types.ASSET)
      //   rutils.removeSubRoute();
      this.handleNavigateRoute();
    }

  }

  getWindowData(id) {
    return this.state.windowList.find((w) => w.id === id);
  }

  openFloatingMenu() {
    this.setState(prevState =>({
      isFloatingMenuOpen: (!prevState.isFloatingMenuOpen),
    }));
  }

  // findWindowOnStack(id) {
  //   return this.state.windowList.find((wId) => wId === id);
  // }

  render() {
    const {
      windowList,
      windowStack,
      activeWindow,
      activePath,
      isMobile,
      isFloatingMenuOpen,
    } = this.state;

    const { data } = this.props;
    return (
      <div className="Desktop">
        <WindowManager
          data={data}
          windowList={windowList}
          windowStack={windowStack}
          activeWindow={activeWindow}
          handleOpenWindow={this.handleOpenWindow}
          handleCloseWindow={this.handleCloseWindow}
          handleFocusWindow={this.handleFocusWindow}
          isMobile={isMobile} />
        <FloatingMenu
          handleOpenWindow={this.handleOpenWindow}
          toggle={this.openFloatingMenu}
          activeWindow={activeWindow}
          isWindowStackEmpty={(windowStack.length == 0)}
          isMobile={isMobile}
          isOpen={isFloatingMenuOpen} />
        <StatusBar
          path={activePath}
          handleOpenWindow={this.handleOpenWindow}
          openFloatingMenu={this.openFloatingMenu.bind(this)}
          isWindowStackEmpty={(windowStack.length == 0)}
          isMobile={isMobile} />
      </div>
    );
  }
}

export default Desktop;

Desktop.propTypes = {
  data: PropTypes.array.isRequired,
}
