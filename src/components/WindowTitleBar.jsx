import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

function WindowTitleBar(props) {
  const { titleBarRef, onWindowClose, title, subWindow} = props;

  let modifierClass = '';
  if (subWindow) modifierClass = ' Window__TitleBar--subWindow'
  return (
    <div className={`Window__TitleBar${modifierClass}`} ref={titleBarRef}>
      <div className="Window__Controls">
        <div className="Window__Close" onClick={onWindowClose}>
          <Icon icon="close" />
        </div>
        <div className="Window__Back" onClick={onWindowClose}>{`<-`}</div>
      </div>
      <div className="Window__Title">{title}</div>
    </div>
  );
}

export default WindowTitleBar;

WindowTitleBar.propTypes = {
  titleBarRef: PropTypes.func,
}
