import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import * as Projects from './Projects';
import Window from './Window';
import { unsetDraggable } from '../utilities/setDraggable';
import WINDOW_DATA from '../data/windowData';
// const p1Promise = import("./Projects/Project1");
// const Project1 = React.lazy(() => p1Promise);

class WindowManager extends Component {
  constructor(props) {
    super(props);

    // this.P1import = import('./Projects/Project1');
    // this.P1;


    this.renderWindowContent = this.renderWindowContent.bind(this);
    this.getWindowIndex = this.getWindowIndex.bind(this);
    this.preloadWindow = this.preloadWindow.bind(this);
  }

  preloadWindow(factory) {
    const Component = React.lazy(factory);
    Component.preload = factory;
    return Component;
  }

  componentDidMount() {
    // this.P1 = React.lazy(() => this.P1import);
    // this.P1.preload();
    // console.log(Project1);
  }

  renderWindowContent(w) {
    // const { handleOpenWindow } = this.props;
    const { handleCloseWindow, handleFocusWindow, handleOpenWindow, activeWindow, isMobile } = this.props;
    const newProps = {
      handleCloseWindow: handleCloseWindow,
      orderIndex: this.getWindowIndex(w.id),
      handleFocusWindow: handleFocusWindow,
      handleOpenWindow: handleOpenWindow,
      data: w,
      isMobile: isMobile,
    };
    // const P1 = this.P1;
    switch(w.id) {
      case 'p1':
        return <Projects.p1 data={w} {...newProps} />
      case 'p2':
        return <Projects.p2 data={w} {...newProps} />
      case 'p3':
        return <Projects.p3 data={w} {...newProps} />
      default:
        return null;
    }
  }

  getWindowIndex(id) {
    const { windowStack } = this.props;
    return windowStack.findIndex((wId) => wId === id);
  }

  render() {
    const {
      windowList,
      windowStack,
      handleCloseWindow,
      activeWindow,
      handleFocusWindow,
      isMobile
    } = this.props;
    let keyModifier = '';
    if (isMobile) {
      keyModifier = '-mobile'; // anything here, to force a remount
    }

    return (
      <>
        {windowList.map(w => (
          <React.Fragment key={`${w.id}${keyModifier}`}>
            <Window onWindowClose={handleCloseWindow}
                    data={w}
                    isActive={(activeWindow.id == w.id)}
                    orderIndex={this.getWindowIndex(w.id)}
                    handleFocusWindow={handleFocusWindow}
                    isMobile={isMobile}>
              {this.renderWindowContent(w)}
            </Window>
          </React.Fragment>
        ))}
        {/* TODO: Preload images */}
        <div style={{display: 'none'}}>
          <Projects.p1 />
        </div>
      </>
    );
  }
}

export default WindowManager;
