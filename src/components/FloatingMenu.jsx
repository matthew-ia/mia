import React, { Component } from 'react';
import Icon from './Icon';
import setDraggable, { unsetDraggable, isInteractable } from '../utilities/setDraggable';
import * as animate from '../utilities/animate';

class FloatingMenu extends Component {
  constructor(props) {
    super(props);

    // Set Refs
    this.windowRef = React.createRef();
    this.titleBarRef = null;
    this.setTitleBarRef = (element) => {
      this.titleBarRef = element;
    };

    this.handleOpenWindow = this.handleOpenWindow.bind(this);
  }

  componentDidMount() {
    // Using the refs, set the Window to be draggable
    // using WindowTitleBar as its drag handle
    if (!this.props.isMobile)
      setDraggable(this.windowRef.current, this.titleBarRef);
    else
      unsetDraggable(this.windowRef.current);
      // setDraggable(this.windowRef.current, this.titleBarRef, true);
  }

  handleOpenWindow(id) {
    const { activeWindow, handleOpenWindow, toggle, isMobile, isOpen } = this.props;
    let options = { onlyOne: false };
    if (isMobile) options.onlyOne = true;
    // If active, pulse window
    if (activeWindow.id === id) {
      animate.pulseWindow(id);
      handleOpenWindow(id, options);
    } else {
      handleOpenWindow(id, options);
    }
    // Toggle Off if open
    if (this.props.isMobile && this.props.isOpen) {
      this.props.toggle();
    }
  }

  render() {
    const { handleOpenWindow, isOpen, isMobile, activeWindow } = this.props;
    let modifierClass = '';

    let node = document.querySelector('.FloatingMenu');
    if (isMobile && node) unsetDraggable(document.querySelector('.FloatingMenu'));

    const { isWindowStackEmpty } = this.props;
    if (isWindowStackEmpty && isMobile) {
      // If it's mobile, and there are no other windows, show it
      // in it's altered styling
      console.log('this bitch EMPTY');
      modifierClass = ' FloatingMenu--alwaysOpen';
    } else if (isMobile && isOpen) {
      // If it's mobile and opened, but no empty stack, show normal slide-up
      modifierClass = ' FloatingMenu--mobile';
    } else if (isMobile && !isOpen) {
      // Hide class if it has been set to be toggled off
      if (node.classList.contains('FloatingMenu--mobile')) {
        modifierClass = ' FloatingMenu--hiding';
      }
    }


    return (
      <div className={`FloatingMenu${modifierClass} Window`} ref={this.windowRef}>
        <div className="Window__TitleBar" ref={this.setTitleBarRef}>
          <div className="Window__Title">Files</div>
          <div className="Window__Controls">
            {/* <div className="Window__Close" onClick={handleClose}>x</div> */}
          </div>
        </div>
        <ul className="FloatingMenu__List">
          <li
            className="FloatingMenu__ListItem"
            onClick={this.handleOpenWindow.bind(this, 'p1')}>
              <span className="FloatingMenu__ListItemInner">
                <Icon icon="folder" className="red" />
                <span>Maldek</span>
                <i>-></i>
              </span>
          </li>
          <li
            className="FloatingMenu__ListItem"
            onClick={this.handleOpenWindow.bind(this, 'p2')}>
              <span className="FloatingMenu__ListItemInner">
                <Icon icon="folder" className="blue" />
                <span>PM Design</span>
                <i>-></i>
              </span>
          </li>
          <li
            className="FloatingMenu__ListItem"
            onClick={this.handleOpenWindow.bind(this, 'p3')}>
              <span className="FloatingMenu__ListItemInner">
                <Icon icon="folder" className="yellow" />
                <span>APPTEL</span>
                <i>-></i>
              </span>
          </li>
          <li
            className="FloatingMenu__ListItem"
            onClick={this.handleOpenWindow.bind(this, 'wInfo')}>
              <span className="FloatingMenu__ListItemInner">
                <Icon icon="page" className="gray" />
                <span>About</span>
                <i>-></i>
              </span>
          </li>
          <li
            className="FloatingMenu__ListItem"
            onClick={this.handleOpenWindow.bind(this, 'wContact')}>
              <span className="FloatingMenu__ListItemInner">
                <Icon icon="page" className="gray" />
                <span>Contact</span>
                <i>-></i>
              </span>
          </li>
        </ul>
      </div>
    );
  }

}

export default FloatingMenu;
