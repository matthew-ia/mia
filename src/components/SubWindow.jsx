import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import WindowTitleBar from './WindowTitleBar';

import setDraggable from '../utilities/setDraggable';

class SubWindow extends Component {
  constructor(props) {
    super(props);

    // Set Refs
    // this.windowRef = React.createRef();
    // this.titleBarRef = null;
    // this.setTitleBarRef = (element) => {
    //   this.titleBarRef = element;
    // };
  }

  componentDidMount() {
    // Using the refs, set the Window to be draggable
    // using WindowTitleBar as its drag handle
    // setDraggable(this.windowRef.current, this.titleBarRef);
  }

  render() {
    const {
      children,
      onWindowClose,
      src,
      parentData,
      isLandscape,
      isPortrait
    } = this.props;

    // Modifier Class for content ratio
    let modifierClass;
    if (isLandscape) modifierClass = ' SubWindow--landscape';
    else if (isPortrait) modifierClass = ' SubWindow--portrait';
    else modifierClass = '';

    const parsedSrc = src.split('/').slice(-1)[0];

    this.subWindow = (
      <div className="SubWindow__Backdrop">
        <div
          className={`SubWindow${modifierClass}`}
          ref={this.windowRef}>
          <WindowTitleBar
            onWindowClose={()=>{onWindowClose()}}
            title={`${parentData.name}/${parsedSrc}`}
            subWindow />
          <div>
            {children}
          </div>
        </div>
      </div>
    );

    return ReactDOM.createPortal(
      this.subWindow,
      document.querySelector('.Desktop'),
    );
  }
}

export default SubWindow;

SubWindow.propTypes = {
  children: PropTypes.object, // the Window's content
}

SubWindow.defaultProps = {
  children: {},
}
