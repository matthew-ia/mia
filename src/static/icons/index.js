import React from 'react';

export const close = (
  <svg xmlns="http://www.w3.org/2000/svg" width="20.506" height="20.506" viewBox="0 0 20.506 20.506">
    <g id="Group_167" data-name="Group 167" transform="translate(-1725.718 -78.248)">
      <line id="Line_3" data-name="Line 3" x2="24" transform="translate(1727.486 96.986) rotate(-45)" fill="none" stroke="currentColor" strokeLinecap="round" strokeWidth="2.5"/>
      <line id="Line_4" data-name="Line 4" x2="24" transform="translate(1727.486 80.015) rotate(45)" fill="none" stroke="currentColor" strokeLinecap="round" strokeWidth="2.5"/>
    </g>
  </svg>
);

export const folder = (
  <svg xmlns="http://www.w3.org/2000/svg" width="27.35" height="20.482" viewBox="0 0 27.35 20.482">
    <g id="Folder" transform="translate(1 0.5)">
      <path id="Tab" d="M18.458,4.353,16.364.682A.778.778,0,0,0,15.743.34H3.817a.829.829,0,0,0-.635.342L1.16,4.353Z" transform="translate(-0.221 -0.34)" fill="#fff" stroke="#000" strokeMiterlimit="10" strokeWidth="1"/>
      <g id="Main" transform="translate(0 4.013)" fill="#fff" stroke="#000" strokeMiterlimit="10" strokeWidth="1">
        <rect width="25.35" height="14.969" rx="0.17" stroke="none"/>
        <rect x="-0.5" y="-0.5" width="26.35" height="15.969" rx="0.67" fill="none"/>
      </g>
    </g>
  </svg>
);

export const page = (
  <svg id="Page" data-name="Page" xmlns="http://www.w3.org/2000/svg" width="19.35" height="23.969" viewBox="0 0 19.35 23.969">
    <path id="Background" d="M18.18,23.469H1.17A.671.671,0,0,1,.5,22.8V1.17A.671.671,0,0,1,1.17.5H14.992l.148.161,3.578,3.886.132.144V22.906l-.043.1A.708.708,0,0,1,18.18,23.469Z" fill="#fff"/>
    <path id="Outline" d="M1.17,1A.17.17,0,0,0,1,1.17V22.8a.17.17,0,0,0,.17.17H18.18c.094,0,.17-.17.17-.17V4.886L14.772,1H1.17m0-1h13.6a1,1,0,0,1,.736.323l3.578,3.886a1,1,0,0,1,.264.677V22.8a1,1,0,0,1-.087.409,1.205,1.205,0,0,1-1.083.761H1.17A1.172,1.172,0,0,1,0,22.8V1.17A1.172,1.172,0,0,1,1.17,0Z" fill="#000222"/>
    <path id="Line_3" data-name="Line 3" d="M14.5,15.077H4.5a.5.5,0,0,1,0-1h10a.5.5,0,0,1,0,1Z" fill="#222"/>
    <path id="Line_2" data-name="Line 2" d="M14.5,11.077H4.5a.5.5,0,0,1,0-1h10a.5.5,0,0,1,0,1Z" fill="#222"/>
    <path id="Line_1" data-name="Line 1" d="M14.5,7.077H4.5a.5.5,0,0,1,0-1h10a.5.5,0,0,1,0,1Z" fill="#222"/>
  </svg>
);

export const info = (
  <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
    <g id="Info" transform="translate(0 0)">
      <circle id="Circle" cx="14" cy="14" r="14" transform="translate(1 1)" fill="none" stroke="#fff" strokeWidth="2"/>
      <path id="i" d="M19.924,20.941a4.89,4.89,0,0,1-3.4,1.319c-1.848,0-3.877-.949-3.317-4.067l.58-3.21a4.193,4.193,0,0,1-2.389.68,3.734,3.734,0,0,1-1.509-.27.3.3,0,0,1-.19-.37l.17-1.089c.04-.259.179-.309.38-.259a4.857,4.857,0,0,0,1.169.139,2.869,2.869,0,0,0,2.838-1.459.329.329,0,0,1,.34-.25h1.309c.21,0,.3.13.26.33l-1,5.8c-.31,1.759.709,2.259,1.748,2.259a3.665,3.665,0,0,0,2.189-.821c.18-.13.33-.13.47.13l.4.75A.3.3,0,0,1,19.924,20.941ZM14.267,8.178a1.344,1.344,0,0,1,2.688,0,1.344,1.344,0,1,1-2.688,0Z" fill="#fff"/>
    </g>
  </svg>
);

export const menu = (
  <svg xmlns="http://www.w3.org/2000/svg" width="27" height="22" viewBox="0 0 27 23">
    <g id="Menu" transform="translate(1)">
      <line id="Line_1" data-name="Line 1" x2="25" transform="translate(0 1)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth="2"/>
      <line id="Line_2" data-name="Line 2" x2="25" transform="translate(0 11)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth="2"/>
      <line id="Line_3" data-name="Line 3" x2="25" transform="translate(0 22)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth="2"/>
    </g>
  </svg>
);

export const message = (
  <svg xmlns="http://www.w3.org/2000/svg" width="31.97" height="26.199" viewBox="0 0 31.97 26.199">
    <g id="Message" transform="translate(1 1.412)">
      <g id="Outline">
        <path id="Tail" d="M16.092,19.533v7.178l5.026-4.554" transform="translate(-4.568 -2.926)" fill="none" stroke="#fff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
        <path id="Main" d="M8.577,20.177,38.134,8.169,31.757,30.947,20.1,24.484Z" transform="translate(-8.577 -8.169)" fill="none" stroke="#fff" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
      </g>
      <line id="Line" y1="16.314" x2="18.034" strokeWidth="2" transform="translate(11.524 0)" stroke="#fff" strokeLinecap="round" strokeLinejoin="round" fill="#fff"/>
    </g>
  </svg>
);
