import { default as a1 } from './placeholder-intro.png';
import { default as a2 } from './placeholder-portrait.png';
export const assets = [
  a1,
  a2,
];
