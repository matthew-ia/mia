import interact from 'interactjs';
import drag from './drag';

/**
 * Setup an element to be draggable
 * @param {element} node element to be moved when dragHandle is dragged
 * @param {element} dragHandle the element to activate drag
 */

export default function setDraggable(node, dragHandle, unset) {
  if (unset === true) {
    // console.log(interact(node));
    interact(node).unset();
    return;
  //   // console.log(interact(node));
  }
  // const dragWrapper = () => {drag()};
  interact(node).draggable({
    // enable inertial throwing
    inertia: true,
    // specify a handle to control the drag
    allowFrom: dragHandle,
    // specify an element to avoid controlling drag
    // in your interactable element or handle (allowFrom)
    ignoreFrom: '.Window__Close',
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'parent',
        endOnly: true,
      }),
    ],
    onmove: drag,
  }).styleCursor(false);
}

export function unsetDraggable(node) {

  interact(node).unset();
}

export function isInteractable(node) {
  return interact.isSet(node);
}

// Other options

// == enable autoScroll
//   autoScroll: true,

// == call this function on every dragend event
// onend: function (event) {
//   // do something when done dragging
// }
