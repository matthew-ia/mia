export const dateTimeOptions = {
  hour: '2-digit',
  minute: '2-digit',
  hour12: true,
};

export function getFormattedTime() {
  // Get current time as string
  let time = new Date().toLocaleTimeString([], dateTimeOptions);
  // Convert string into pieces (hour, minute, am/pm)
  time = time.split(/:| /);
  // Remove leading 0 on hour if there is one
  if (time[0][0] == '0') time[0] = time[0].replace(/^0+/, '');
  // Make sure hour cycle is lowercase
  time[2] = time[2].toLowerCase();
  // Return formatted string
  return `${time[0]}:${time[1]} ${time[2]}`;
}
