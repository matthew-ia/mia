import { Router } from './Router';
import { PROJECTS as pData } from '../data/projectData';
import { GENERAL as wData } from '../data/windowData';

const options = { debug: false, startListening: false };
const router = new Router(options)

export function setupRouter() {
  // callback.apply();
  router
  .add(() => {
    // callback();
  });

  // Create routes for all Project Windows
  pData.map((w) => {
    // console.log('yeet', w);
    router.add(`work/${w.name}`, () => {
      handleRoute(`work/${w.name}`);
    });
  });

  // Create routes for all General Windows
  wData.map((w) => {
    // console.log('yeet', w);
    router.add(`${w.name}`, () => {
      handleRoute(`${w.name}`);
    });
  });

  router.add('error', ()=> {
    router.navigate();
  });

  return router;
}

function handleRoute(route) {
  router.validateCurrentRoute();
}

/**
 * Create subroutes for things like SubWindows
 * TODO: consider removing this functionality?
 */
export function addSubRoute(name) {
  router.add(`${router.currentRoute}/${name}`);
}

export function removeSubRoute() {
  router.remove(`${router.currentRoute}`);
}
