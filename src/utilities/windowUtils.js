
export const types = {
  ASSET: 2,
  PROJECT: 1,
  GENERAL: 0,
}

export function getWindowType(id) {
  console.log(id, id[0]);
  switch(id[0]) {
    case 'p':
      return 1;
    case 'a':
      return 2;
    default:
      return 0;
  }
}

export function getWindowData(data, id) {
  return data.find((w) => w.id === id);
}

export function generateWindowData(id) {
  let data = {
    id: id,
    title: id,
    name: id,
  }
  return data;
}
