
export function pulseWindow(id) {
  let windowEl = document.querySelector(`#${id}`)
  let t = windowEl.style.transform;
  let newT = `${t} scale(1.02)`;
  windowEl.style.transition = 'transform 0.2s ease-out';
  windowEl.style.transform = newT;
  windowEl.classList.add('Window--activePulse');
  setTimeout(()=>{
    windowEl.style.transform = `${t} scale(1)`;
  }, 200);
  setTimeout(()=>{
    windowEl.style.transform = t;
    windowEl.style.transition = '';
    windowEl.classList.remove('Window--activePulse');
  }, 500);
}
