import { assets, placeholders } from '../static/img';

/**
 * Pre-load imported assets
 */
export default function preload() {
  loadImages(assets);
  if (placeholders) {
    loadImages(placeholders);
  }
}

function loadImages(sources) {
  for (let src of sources) {
    const img = new Image();
    img.src = src;
  }
}
