import { PROJECTS } from './projectData';

export const GENERAL = [
  {
    id: 'wInfo',
    title: 'About',
    name: 'about',
    // tags: ['ui', 'design', 'web'],
    desc: 'desc 1',
  },
  {
    id: 'wContact',
    title: 'contact',
    name: 'contact',
    // tags: ['ui', 'design', 'web'],
    desc: 'desc 1',
  },
];


export const WINDOW_DATA = [...GENERAL, ...PROJECTS]
