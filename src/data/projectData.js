export const PROJECTS = [
  {
    id: 'p1',
    title: 'project',
    name: 'project-1',
    tags: ['ui', 'design', 'web'],
    desc: 'desc 1',
  },
  {
    id: 'p2',
    title: 'p2.proj',
    name: 'project-2',
    tags: ['ui', 'design', 'web'],
    desc: 'desc 2',
  },
  {
    id: 'p3',
    title: 'p3.proj',
    name: 'project-3',
    tags: ['ui', 'design', 'web'],
    desc: 'desc 3',
  },
]
