
export const PROJECT_MAP = {
  p1: {
    name: 'Project 01',
    shortname: 'project_01',
    tags: 'ui,design,web',
    info: 'desc 1',
    more: 'more info',
    assets: {
      a1: 'placeholder-img-1.png',
      a2: 'placeholder-img-2.png',
      a3: 'placeholder-img-3.png',
      a4: 'placeholder-img-4.png',
    }
  },
  p2: {
    name: 'Project 02',
    shortname: 'project_02',
    tags: 'tag_1,tag_2,tag_3',
    info: 'desc 2',
    more: 'more info',
    a1: 'placeholder-img-1.png',
    a2: 'placeholder-img-2.png',
    a3: 'placeholder-img-3.png',
    a4: 'placeholder-img-4.png',
  },
  p3: {
    name: 'Project 03',
    shortname: 'project_03',
    tags: 'tag_1,tag_2,tag_3',
    info: 'desc 3',
    more: 'more info',
    a1: 'placeholder-img-1.png',
    a2: 'placeholder-img-2.png',
    a3: 'placeholder-img-3.png',
    a4: 'placeholder-img-4.png',
  },
  p4: {
    name: 'Project 04',
    shortname: 'project_04',
    tags: 'tag_1,tag_2,tag_3',
    info: 'desc 4',
    more: 'more info',
    a1: 'placeholder-img-1.png',
    a2: 'placeholder-img-2.png',
    a3: 'placeholder-img-3.png',
    a4: 'placeholder-img-4.png',
  },
  p5: {
    name: 'Project 05',
    shortname: 'project_05',
    tags: 'tag_1,tag_2,tag_3',
    info: 'desc 5',
    more: 'more info',
    a1: 'placeholder-img-1.png',
    a2: 'placeholder-img-2.png',
    a3: 'placeholder-img-3.png',
    a4: 'placeholder-img-4.png',
  },
  p6: {
    name: 'Project 06',
    shortname: 'project_06',
    tags: 'tag_1, tag_2, tag_3',
    info: 'desc 6',
    more: 'more info',
    a1: 'placeholder-img-1.png',
    a2: 'placeholder-img-2.png',
    a3: 'placeholder-img-3.png',
    a4: 'placeholder-img-4.png',
  }
}
