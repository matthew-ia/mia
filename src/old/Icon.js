
import { PROJECT_MAP } from './projectMap';

export class Icon {
  constructor(view, desktop, projectMap) {
    this.view = view;
    this.desktop = desktop;
    this.pMap = projectMap;
    this.overlay;
  }

  setup() {
    // Load in Dynamic Info
    this.view.querySelector('.title').innerText =
      this.pMap[this.view.dataset.key].shortname;
    this.view.querySelector('.info').innerText =
      this.pMap[this.view.dataset.key].info;

    // Setup Listeners
    //== Open Window
    this.view.addEventListener('click', () => {
      this.desktop.openWindow(this.view.dataset.key);
    });
  }
}
