import { Window } from './Window';
import { Icon } from './Icon';
export class Desktop {
  constructor(projectMap, routerRef) {
    this.view = document.querySelector('#desktop');
    this.windows = []; // The Window stack, in order of last active
    this.icons = []; // TEMP: List of icons on the desktop
    this.pMap = projectMap;
    this.router;
    this.setup();
  }

  setup() {
    this.setupIcons();
    this.setupWindows();
    this.setupStatusBar();
  }

  // FIXME: this is a mess!!!
  setupIcons() {
    let iconArray = document.querySelectorAll('#desktop .desktop-grid .icon');
    for (let i of iconArray) {
      let icon = new Icon(i, this, this.pMap);
      icon.setup();
      this.icons.push(icon);
    }
  }

  setupWindows() {
    //FIXME: don't do this, bc SEO could benefit from hardwritten in markup ?
    let windows = document.querySelectorAll('.window');
    for (let w of windows) {
      // Handle Special Windows
      if (w.id == 'about') {
        w.dataset.name = 'about';
      }
      // Handle Project Windows
      let p = this.pMap[w.id];
      if (p === undefined) continue;
      let titleBarTitle = w.querySelector('.title-bar .title');
      let headerTitle = w.querySelector('.header .title');

      titleBarTitle.innerText = p.shortname;
      headerTitle.innerText = p.name;
      w.dataset.name = p.shortname;
    }
  }

  setupStatusBar() {
    let messageBtn = this.view.querySelector('#statusbar .btn.message');
    let infoBtn = this.view.querySelector('#statusbar .btn.info');
    tippy.setDefaults({
      content: 'tooltip',
      animation: 'fade',
      animateFill: false,
      delay: 250,
      theme: 'desktop',
      touch: false,
    });
    tippy(messageBtn, {
      content: 'Email',
    });
    tippy(infoBtn, {
      content: 'About',
    });
    messageBtn.addEventListener('click', ()=>{
      messageBtn.querySelector('a').click();
    });
    infoBtn.addEventListener('click', ()=>{
      this.openWindow('about');
    });
  }

  setRouter(routerRef) {
    this.router = routerRef;
  }
  /**
   * Gets an copy of the current state of the Windows that are currently open on the Desktop
   * @return {Array} list of open Window objects
   */
  getWindows() {
    return Object.assign({}, this.windows);
  }

  getActiveWindow() {
    //console.log("gettng active", this.windows);
    let w = this.windows[this.windows.length-1];
    if (w) return w;
    else return -1;
  }

  /**
   * Opens a Window on the Desktop and updates window array
   *    Also sets an already open window to the active window
   * @return none
   */
  openWindow(id) {
    // Toggle current active window if exists and is not the window to be opened

    let aw = this.getActiveWindow();
    let i = this.windows.findIndex(w => w.id == id);
    if (i == -1) { // Doesn't exist, so create it
      if (document.getElementById(id) == null) {
        console.log(`[Window:Error] Window with id '${id}' does not exist.`);
        return;
      }
      document.getElementById(id).style = ''; // Reset position styling
      let w = new Window(id, document.getElementById(id), this);
      this.windows.push(w);
      w.open();
      w.toggleActive();
      if (aw != -1) {
        aw.toggleActive();
      }
    } else { // Does exist, so check if its at top of stack, otherwise reorder
      // if active but not the window to open
      if (aw != -1 && aw.id != id) {
        aw.toggleActive();
        this.setActiveWindow(i);
      }
      // if not active, but open, and the window to open
      else if (!this.windows[i].isActive) {
        this.setActiveWindow(i);
      }
    }
    this.update(id);
  }

  /**
   * Closes a Window on the Desktop
   *    and removes it from the Window stack, and toggles the next stack top
   */
  closeWindow(id) {
    let i = this.windows.findIndex(w => w.id == id);
    if (i == -1) {
      console.log(`[Window:Error] no window with id '${id}'`);
    } else {
      let w = Object.assign({}, this.windows[i]);
      this.windows.splice(i, 1);
      if (this.windows.length -1 >= 0)
        this.windows[this.windows.length - 1].toggleActive(); // open next window

      // only if type IMAGE, remove it from DOM
      if (document.querySelector(`#${w.id}`).classList.contains('image'))
        this.view.removeChild(document.querySelector(`#${w.id}`));
    }
    this.update();
  }

  setActiveWindow(index) {
    let w = this.windows[index];
    this.windows.splice(index, 1);
    w.toggleActive();
    this.windows.push(w);
  }

  openSubWindow(type, index, target) {
    // Window View data
    let id = `${target.id}-view`;
    // Check if Window already exists
    if (document.getElementById(id)) return;
    let title = target.name;
    let windowTitle = id;
    let asset, p, a, content;

    // Setup Window View element
    let wElement = document.createElement('div');
    wElement.id = id;
    wElement.dataset.name = title;
    console.log(this.getActiveWindow());
    wElement.dataset.parentName = this.getActiveWindow().node.dataset.name;
    wElement.classList.add('window', type);
    // Create File Map Keys and Asset Element
    if (type == 'image') {
      asset = target.id.split('-');
      p = asset[0];
      a = 'a' + asset[1];
      content = `<img src='./img/${this.pMap[p].assets[a]}' />`;
    } else {
      content = '';
    }

    // Create Window
    wElement.innerHTML =
      '<div class="title-bar">' +
        `<div class="title">${title}</div>` +
        '<div class="controls">' +
          '<div class="close"></div>' +
        '</div>' +
      '</div>' +
      '<div class="content">' +
        content +
      '</div>'
    ;

    // Load View into Desktop and open Window
    desktop.appendChild(wElement);
    this.openWindow();
  }

  /**
   * Updates the statusbar text to show the 'cwd'
   * @return none
   */
  updateStatusBar() {
    let activeWindow = this.getActiveWindow();
    //console.log(activeWindow);
    let cwd = '';
    if (activeWindow == -1) {
      console.log('status: desktop');
      cwd = `/ work`;
    } else {
      let wName;
      if (activeWindow.id[0] == 'p') {
        if (activeWindow.node.classList.contains('image')) {
          wName = activeWindow.node.dataset.name;
          let pName = activeWindow.node.dataset.parentName;
          cwd = `/ work / ${pName} / ${wName}`;
        } else {
          wName = this.pMap[activeWindow.id].shortname.toLowerCase();
          cwd = `/ work / ${wName}`;
        }

      } else if (activeWindow.id == 'about') {
        // TODO: make sure this case works
        wName = activeWindow.node.dataset.name;
        cwd = `/ info / ${wName}`;
      }
    }
    this.view.querySelector('.powerCwd span').innerText = `${cwd}`;
    this.updateRoute(cwd);
  }

  updateWindowPositions() {
    for (let i = this.windows.length-1; i >= 0; i--) {
      this.windows[i].node.style.zIndex = i+1;
    }
  }

  updateRoute(route) {
    if (this.router == undefined) return;
    route = route.split(' ').join('').split('/');
    if (route[2] == undefined) this.router.navigate(`${route[1]}`);
    else this.router.navigate(`${route[1]}/${route[2]}`);
  }

  /**
   * Updates the Desktop
   *    Windows, and status bar
   * @return none
   */
  update() {
    console.log('updating');
    this.updateWindowPositions();
    this.updateStatusBar();
    //this.updateRoute(routeID);
  }
}
