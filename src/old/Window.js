import { Draggable } from './Draggable';

export class Window extends Draggable {
  constructor(id, node, desktop) {
    super(id, node, document.getElementById('desktop'));
    this.id = id;
    this.node = node;
    this.desktop = desktop;
    this.isActive = false;
    this.init();
  }

  init() {
    //** setActive listener
    this.node.addEventListener('click', () => {
      if (!this.isActive) {
        this.desktop.openWindow(this.id);
      }
    });
    // TODO: adapt this to work for opening new windows
    // at offsets
    if (this.id == 'about') {
      let rect = this.node.getBoundingClientRect();
      console.log(this.node, rect);
      console.log(window.innerWidth, window.innerHeight);
      let leftOffset = window.innerWidth * .5;
      leftOffset = leftOffset - rect.width / 2;
      let topOffset = (window.innerHeight * .5) - document.querySelector('#statusbar').getBoundingClientRect().height;
      console.log('1', topOffset);
      topOffset = topOffset - (rect.height / 2);
      console.log('2', topOffset);
      console.log('ABOUT', leftOffset, topOffset);
      this.node.style.left = leftOffset + "px";
      this.node.style.top = topOffset + "px";
    }

    //** Control Button listeners
    // Close control
    let closeOnce = (e) => {
      this.toggleActive();
      this.close();
      this.desktop.closeWindow(this.id);
      this.node.querySelector('.close').removeEventListener('click', closeOnce);
      e.stopPropagation(); // Must include
    };
    this.node.querySelector('.close').addEventListener('click', closeOnce);

    // Max and Min controls
    let controlMax = this.node.querySelector('.max');
    let controlMin = this.node.querySelector('.min');
    if (controlMax !== null) {
      controlMax.addEventListener('click', (event) => {
        this.maximize();
      });
    }
    if (controlMin !== null) {
      controlMin.addEventListener('click', (event) => {
        this.minimize();
      });
    }

    //** Listeners to open Subwindows
    if (this.node.classList.contains('project')) {
      let images = this.node.querySelectorAll('.content img');
      let type = 'image';
      //console.log(images);
      let c = 0; // count
      for (let i = 0; i < images.length; i++) {
        let handler = (e) => {
          this.openSubWindow(type, i, e.target)
          //images[i].removeEventListener('click', handler);
          e.stopPropagation();
          // console.log('did it work?');
        };
        images[i].addEventListener('click', handler);
      }
    }
  }

  /**
   * Handle the UI state for opening a Window
   * @return none
   */
  open() {
    this.node.classList.add('open');
  }

  /**
   * Handle the UI state for closing a window
   * @return none
   */
  close() {
    this.node.classList.remove('open', 'min', 'max');
  }

  maximize() {
    if (this.node.classList.contains('min')) {
      this.node.classList.remove('min');
    } else {
      this.node.classList.add('max');
    }
  }

  minimize() {
    if (this.node.classList.contains('max')) {
      this.node.classList.remove('max');
    } else {
      this.node.classList.add('min');
    }
  }

  openSubWindow(type, index, target) {
    this.desktop.openSubWindow(type, index, target);
  }

  toggleActive() {
    // If off, toggle on
    if (!this.isActive) {
      this.isActive = true;
      let prev = document.querySelector('.window.active');
      if (prev && prev !== this.node) prev.classList.remove('active');
      this.node.classList.add('active');
    } else {
      this.isActive = false;
      this.node.classList.remove('active');
    }
  }
}
