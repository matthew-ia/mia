// ** Various Tools!!!!
//

const PREFIX = '[TOOLS]: ';
const SHOW_LOG = false;
const log = function(...args) {
  if (SHOW_LOG) console.log(PREFIX, ...args);
  else return;
}

export function replaceSVG() {
  let imgSVG = document.querySelectorAll('img.svg');
  let src;
  for (let i of imgSVG) {
    src = i.src.split('/');
    for (let j = 0; j < src.length; j++) {
      if (src[j] == 'img') {
        src = src.slice(i);
        src = src.join('/');
        break;
      }
    }
    getAsync(src, (svg) => {
      log(svg);
      const parser = new DOMParser();
      const xmlDoc = parser.parseFromString(svg, 'text/html');
      let svgEl = xmlDoc.querySelector('svg');
      log(svgEl);
      i.parentNode.replaceChild(svgEl, i);
    });
  }
}

function getAsync(url, callback) {
  let http = new XMLHttpRequest();
  http.onreadystatechange = () => {
    if (http.readyState == 4 && http.status == 200) {
      callback(http.responseText);
    }
  }
  http.open("GET", url, true); // false for synchronous request
  http.send(null);
}

/**
document.querySelectorAll('img.svg').forEach((el) => {
  const imgID = el.getAttribute('id');
  const imgClass = el.getAttribute('class');
  const imgURL = el.getAttribute('src');

  request({url: imgURL}).then((data) => {
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(data, 'text/html');
    let svg = xmlDoc.querySelector('svg');

    if (typeof imgID !== 'undefined') {
      svg.setAttribute('id', imgID);
    }

    if(typeof imgClass !== 'undefined') {
      svg.setAttribute('class', imgClass + ' replaced-svg');
    }

    svg.removeAttribute('xmlns:a');

    el.parentNode.replaceChild(svg, el);
  })
});




const http: XMLHttpRequest = new XMLHttpRequest();
        http.open("GET", url, true);
        http.setRequestHeader("Content-Type", "text/xml; charset=UTF-8");
        http.onload = () => {
            if(http.readyState === http.DONE && http.status === 200) {
                const target: HTMLElement = http.responseXML.documentElement;
                this.replace(source, target);
            }
        };
        http.send(null);
*/
