import interact from 'interactjs';

export class Draggable {
  constructor(id, node, container) {
    // Main Class Variables
    this.id = id;
    this.node = node;
    this.dragHandle = this.node.querySelector('.title-bar');
    this.container = container; // the workspace or desktop
    // Position Variables
    this.active = false;
    this.currentX;
    this.currentY;
    this.initialX;
    this.initialY;
    this.xOffset = 0;
    this.yOffset = 0;
    // Helper
    this.dragStartRef;
    this.dragEndRef;
    this.dragRef;

    // Initialize event listeners to make Draggable work
    this.initDraggable();
  }

  initDraggable() {
    // let self = this;
    // // console.log("DRAG HANDLE", this.dragHandle);
    // this.dragHandle.addEventListener('mousedown', () => {
    //   self.dragStart(event, self);
    // });
    interact(this.node).draggable({
      // enable inertial throwing
      inertia: true,
      // keep the element within the area of it's parent
      modifiers: [
        interact.modifiers.restrictRect({
          restriction: this.container,
          endOnly: true
        })
      ],
      // enable autoScroll
      //autoScroll: true,
      // call this function on every dragmove event
      onmove: this.dragMoveListener,
      // call this function on every dragend event
      // onend: function (event) {
      //   // do something when done dragging
      // }
    });
  }
  dragMoveListener (event) {
    var target = event.target
    // keep the dragged position in the data-x/data-y attributes
    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
    var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

    // translate the element
    target.style.webkitTransform =
      target.style.transform =
        'translate(' + x + 'px, ' + y + 'px)'

    // update the posiion attributes
    target.setAttribute('data-x', x)
    target.setAttribute('data-y', y)
  }
}
