import { Desktop } from './Desktop';
import { replaceSVG } from './tools';
import { PROJECT_MAP } from './projectMap';
import { Router } from './Router';

let desktop = new Desktop(PROJECT_MAP);
let router = setupRouter();
desktop.setRouter(router);
// desktop.openWindow('p2');
// desktop.openWindow('p1');

let statusBarTime = document.querySelector('#statusbar .time');

function updateTime() {
  let time = new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
  document.querySelector('#statusbar .time').innerText = time;
  let t = setTimeout(updateTime, 3000);
}
updateTime();

replaceSVG();

function setupRouter() {
  let hashRoute = window.location.hash.substring(1);
  console.log(hashRoute);
  if (hashRoute[hashRoute.length - 1] == '/') {
    hashRoute = hashRoute.substring(0, hashRoute.length-1);
    console.log(hashRoute, 'had /');
  }

  let router = new Router();
  //let hashRoute = router.currentRoute;
  router.add('work', () => {
  });
  let pMapKeys = Object.keys(PROJECT_MAP);
  console.log(pMapKeys);
  for (let p of pMapKeys) {
    let route = `work/${PROJECT_MAP[p].shortname}`;
    router.add(route.toLowerCase(), () => {
      console.log('going to route', p);
    });
  }
  console.log(router.routes);
  // Get just the route path without the hash (#)
  //let hashRoute = window.location.hash.substring(1);
  if (hashRoute != '') {
    let isValid = false;
    for (let r of router.routes) {
      if (hashRoute == `/${r.pathString}`) isValid = true;
    }
    if (hashRoute == 'work') isValid = true;
    if (isValid) {
      // Parse route
      let wPath = hashRoute.split('/');
      // Open respective window if applicable
      if (wPath[1] == 'work') {
        if (wPath[2]) {
          for (let p of pMapKeys) {
            let wName = PROJECT_MAP[p].shortname;
            if (wName == wPath[2]) {
              desktop.openWindow(p);
              router.navigate(hashRoute);
            }
          }
        } else {
          router.navigate(hashRoute);
        }
      } else if (wPath[1] == 'info') {
        desktop.openWindow('info');
        router.navigate(hashRoute);
      }
    } else { // invalid URL route
      router.navigate('work');
      console.log("invalid or blank path");
    }
  } else if (hashRoute == '' || hashRoute == 'error') router.navigate('work');
  return router;
}

desktop.openWindow('about');

function handleRoute(toRoute) {
  router.navigate(toRoute);
}


// runTestDesktop();
// runWindowTest();
