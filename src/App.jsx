import { hot } from 'react-hot-loader';
import React from 'react';
import Desktop from './components/Desktop';

import { WINDOW_DATA } from './data/windowData';

const App = () => (
  <Desktop data={WINDOW_DATA} />
);

export default hot(module)(App);
